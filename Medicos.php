<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Buttons</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-hospital-symbol"></i>
                </div>
                <div class="sidebar-brand-text mx-3">HOSPITAL</div>
            </a>
            <hr class="sidebar-divider my-0">
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Menú Prinicipal
            </div>
            <li class="nav-item active">
                <a class="nav-link" href="Medicos.php">
                    <i class="fas fa-user-md"></i>
                    <span>Medicos</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="hospitales.php">
                    <i class="far fa-hospital"></i>
                    <span>Hospitales</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="pacientes.php">
                    <i class="fas fa-user-injured"></i>
                    <span>Pacientes</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="servicios.php">
                    <i class="fas fa-concierge-bell"></i>
                    <span>Servicios</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="visitas.php">
                    <i class="fas fa-notes-medical"></i>
                    <span>Visitas Médicas</span></a>
            </li>
         
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
    
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar Médico" aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <div class="topbar-divider d-none d-sm-block"></div>
                    </ul>
                </nav>
                <div class="container-fluid">
                    <h1 class="h3 mb-4 text-gray-800">Doctores</h1>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Alta de Médicos</h6>
                                </div>
                                <div class="card-body">
                                    <!--Formulario de alta medicos-->
                                    <form class="user" action="RegistarMedicos.php" method="POST">
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <input type="text" class="form-control form-control-user" id="exampleFirstName" placeholder="DNI" name="nm_dni">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="date" class="form-control form-control-user" id="exampleLastName" placeholder="Fecha" name="nm_fecha">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" id="exampleInputEmail" placeholder="Nombre" name="nm_nombres">
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <input type="text" class="form-control form-control-user" id="exampleInputPassword" placeholder="Apellido Paterno" name="apellido1">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Apellido Materno" name="apellido2">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                          <label for="">Hospital Adscrito</label>
                                           <select name="nm_hospital" id="" class="form-control" >
                                           <option value="0">***********</option>
                                           <?php
                                             include 'conexion.php';
                                              $query = $conexion -> query ("SELECT * FROM hospitales");
                                              while ($valores = mysqli_fetch_array($query)) {
                                                echo '<option value="'.$valores[Nombre].'">'.$valores[Nombre].'</option>';
                                              }
                                            ?>
                                            </select>
                                        </div>

                                         <div class="form-group">
                                          <label for="">Cargo</label>
                                           <select name="nm_cargo" id="" class="form-control" >
                                           <option value="0">***********</option>
                                           <?php
                                             include 'conexion.php';
                                              $query = $conexion -> query ("SELECT * FROM cargos");
                                              while ($valores = mysqli_fetch_array($query)) {
                                                echo '<option value="'.$valores[nombre].'">'.$valores[nombre].'</option>';
                                              }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                          <label for="">Servicios</label>
                                           <select name="nm_servicio" id="" class="form-control" >
                                               <option value="0">***********</option>
                                                <?php
                                                    include 'conexion.php';
                                                    $query = $conexion -> query ("SELECT * FROM servicios");
                                                    while ($valores = mysqli_fetch_array($query)) {
                                                        echo '<option value="'.$valores[Nombre].'">'.$valores[Nombre].'</option>';
                                                }
                                                ?>
                                           </select>
                                        </div>
                                        <input type="submit" value="REGISTRAR" name="RegMedicos" class="btn btn-primary btn-user btn-block"/>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">

                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Mostrar Médicos</h6>
                                </div>
                                <div class="card shadow mb-4">

                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" name="">
                                            
                                            <?php 
                                            include 'conexion.php';
                                            $insertar = "SELECT * FROM medicos";
                                            $resultado = mysqli_query ($conexion, $insertar);
                                            echo "
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>DNI</th>
                                                    <th>Nombre</th>
                                                    <th>AP Paterno</th>
                                                    <th>AP Materno</th>
                                                    <th>Fecha Nacimiento</th>
                                                    <th>Hospital</th>
                                                    <th>Servicio</th>
                                                    <th>Cargo</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>";
                                            
                                            while ($mostrar = $resultado -> fetch_row())
                                            {
                                                echo "<thead>
                                                <td>
                                                    $mostrar[0]        
                                                </td>
                                                <td>
                                                    $mostrar[1]          
                                                </td>
                                                <td>
                                                    $mostrar[2]        
                                                </td>
                                                <td>
                                                    $mostrar[3]          
                                                </td>
                                                <td>
                                                    $mostrar[4]        
                                                </td>
                                                <td>
                                                    $mostrar[5]          
                                                </td>
                                                <td>
                                                    $mostrar[6]        
                                                </td>
                                                <td>
                                                    $mostrar[7]        
                                                </td>
                                                <td>
                                                    $mostrar[8]        
                                                </td>
                                                <td>
                                                <form action='eliminarMedicos.php' method='post'><input type='hidden' name='btnEliminar' value='$mostrar[0]'><input type='submit' value='Eliminar' class='btn btn-primary btn-user btn-block'/></form> 
                                                </td>
                                                <td>
                                                <a href='editarMedicos.php'><input type='submit' value='Editar' name='btnEditar'  class='btn btn-primary btn-user btn-block'/></a>     
                                                </td>
                                            </thead>";

                                            }
                                            ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>


                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Hospital Eder and Gus Gus :v</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>

</body>

</html>