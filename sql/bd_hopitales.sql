-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-01-2020 a las 08:39:48
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_hopitales`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id`, `nombre`, `descripcion`) VALUES
(1, 'JEFE', 'Encargado de todas las areas'),
(2, 'Enfermera General', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hospitales`
--

CREATE TABLE `hospitales` (
  `ID` int(11) NOT NULL,
  `CodHospital` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `Ciudad` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `Telefono` int(10) DEFAULT NULL,
  `Direcctor` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Servicios` varchar(35) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hospitales`
--

INSERT INTO `hospitales` (`ID`, `CodHospital`, `Nombre`, `Ciudad`, `Telefono`, `Direcctor`, `Servicios`) VALUES
(1, 'HNP123', 'Niño Poblano', 'Puebla', 123456789, 'Eder', 'Traumatologia'),
(3, 'ytufsdf', 'Hospital De la Mujer', 'Puebla', 123456789, '', '1'),
(4, 'sdfsd', 'sdfsdfsadf', 'Nuevo León', 123456987, 'Raquel', 'NEUROLOGIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicos`
--

CREATE TABLE `medicos` (
  `ID` int(11) NOT NULL,
  `DNI` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `ApellidoPaterno` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `ApellidoMaterno` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `hospital` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `servicio` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cargo` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `medicos`
--

INSERT INTO `medicos` (`ID`, `DNI`, `nombre`, `ApellidoPaterno`, `ApellidoMaterno`, `FechaNacimiento`, `hospital`, `servicio`, `cargo`) VALUES
(4, 'GSG', 'Gustavo', 'Solar', 'Gaona', '1999-07-21', '1', '2', '1'),
(5, 'EDA', 'Eder', 'Dias', 'Andrade', '1858-04-02', '1', '1', '2'),
(7, 'RGA', 'Raquel', 'Dias', 'andrade', '1458-02-21', '1', 'NEUROLOGIA', '1'),
(8, 'SGS', 'agsdia', 'guaisudas', 'uhgsaduih', '1698-05-21', 'Niño Poblano', 'UROLOGIA', 'Enfermera General');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `ID` int(11) NOT NULL,
  `Nombre` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `ApellidoPaterno` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `ApellidoMaterno` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `NoSeguro` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `IdVisita` int(11) DEFAULT NULL,
  `IdServicio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `ID` int(11) NOT NULL,
  `CodServicios` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `Descripcion` text COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`ID`, `CodServicios`, `Nombre`, `Descripcion`) VALUES
(1, 'URO1234', 'UROLOGIA', 'no se que es esto jaja'),
(2, 'NEU56789', 'NEUROLOGIA', 'De las neuronas xdxd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `ID` int(11) NOT NULL,
  `Paciente` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `hospital` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `servicio` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `medico` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tratamiento` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Diagnostico` text COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hospitales`
--
ALTER TABLE `hospitales`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `medicos`
--
ALTER TABLE `medicos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `hospitales`
--
ALTER TABLE `hospitales`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `medicos`
--
ALTER TABLE `medicos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
