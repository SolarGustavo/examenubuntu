<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "bd_hopitales";

// Se crea la conexion
$conexion = mysqli_connect($servername, $username, $password, $database);

// Verifica la conexion
if (!$conexion) {
    die("Conexión Fallida: " . mysqli_connect_error());
}

?>